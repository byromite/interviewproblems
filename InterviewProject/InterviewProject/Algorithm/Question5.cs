﻿using NUnit.Framework;

namespace InterviewProject.Algorithm
{
    [TestFixture]
    public class Question5
    {
        /* Aaron's is running a popularity contest and needs a way to tally votes and decide on a winner
        * a. Write a method that accepts an unordered array of strings, finds the most frequently occuring string, and returns it
        * b. Get BasicTest to pass
        * c. Create additional tests to explore more scenarios
        */
        private static string FindMostPopular(string[] votes)
        {
            
            return "";
        }
        
        [Test]
        public void BasicTest()
        {
            var result = FindMostPopular(new[]
            {
                "John", "Frank", "Jeremy", "Jacob", "Sam", "Jacob", "Sam", "Sam", "Frank", "John", "Sam"
            });

            Assert.AreEqual("Sam", result);
        }

        [Test]
        public void AdditionalTest1()
        {
            var result = FindMostPopular(new[]
            {
                "", "", "", "", ""
            });

            Assert.AreEqual(null, result);
        }

        [Test]
        public void AdditionalTest2()
        {
            var result = FindMostPopular(new[]
            {
                "", "", "", "", ""
            });

            Assert.AreEqual(null, result);
        }

    }
}