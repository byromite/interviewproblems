﻿using NUnit.Framework;

namespace InterviewProject.Algorithm
{
    [TestFixture]
    public class Question1
    {
        // a. Write a method Reverse(string stringToReverse). C# keywords only. Example. HelloWorld -> dlroWolleH
        // b. Get BasicTest to pass
        // c. Create additional tests to explore all scenarios
        // d.  Refactor to use framework and simplify code.
        private static string Reverse(string s)
        {
            return null;
        }
        
        [Test]
        public void BasicTest()
        {
            var result = Reverse("Hello World!");
            
            Assert.AreEqual("!dlroW olleH", result);
        }
        
        [Test]
        public void AdditionalTest1()
        {
            var result = Reverse("");
            
            Assert.AreEqual("", result);
        }
        
        [Test]
        public void AdditionalTest2()
        {
            var result = Reverse("");
            
            Assert.AreEqual("", result);
        }
        
        
    }
}