﻿using NUnit.Framework;

namespace InterviewProject.Algorithm
{
    [TestFixture]
    public class Question2
    {
        // a.  Write a method reverseSentence(string sentenceToReverse). C# keywords only.Example. The Sky Is Blue -> Blue Is Sky The
        // b.  Get BasicTest to pass
        // c. Create additional tests to explore all scenarios
        // d.  Refactor to use framework and simplify code.
        private static string ReverseSentence(string s)
        {
            return null;
        }
        
        [Test]
        public void BasicTest()
        {
            var result = ReverseSentence("The Sky Is Blue");
            
            Assert.AreEqual("Blue Is Sky The", result);
        }
        
        [Test]
        public void AdditionalTest1()
        {
            var result = ReverseSentence("");
            
            Assert.AreEqual("", result);
        }
        
        [Test]
        public void AdditionalTest2()
        {
            var result = ReverseSentence("");
            
            Assert.AreEqual("", result);
        }
    }
}