﻿using NUnit.Framework;

namespace InterviewProject.Algorithm
{
    [TestFixture]
    public class Question3
    {
        // a.  Write a method FindMissingNumber that find a missing number in a list of consecutive numbers.  If the numbers are 1, 2, 3, 5,
        //     then the missing number would be 4.  If there is no missing number, the method returns null.
        // b.  Get BasicTest to pass
        // c.  Create additional tests to explore all scenarios
        private static int? FindMissingNumber(int[] intArray)
        {
            return null;
        }

        [Test]
        public void BasicTest()
        {
            var result = FindMissingNumber(new[] {1, 2, 3, 5});

            Assert.AreEqual(4, result);
        }

        [Test]
        public void AdditionalTest1()
        {
            var result = FindMissingNumber(new int[] {});

            Assert.AreEqual(null, result);
        }

        [Test]
        public void AdditionalTest2()
        {
            var result = FindMissingNumber(new int[] { });

            Assert.AreEqual(null, result);
        }
    }
}