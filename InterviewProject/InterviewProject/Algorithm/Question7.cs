﻿using NUnit.Framework;

namespace InterviewProject.Algorithm
{
    public class Question7
    {
        /*
        * We need to find a missing number in an unordered array of numbers.
        * a. Write a method that accepts an array of positive integers and returns the 1 missing number in the sequence
        * b. Get BasicTest to pass
        * c. Create additional tests to explore more scenarios
        *
        */

        public static int FindMissingNumber(int[] numbers)
        {

            return 0;
        }

        [Test]
        public void BasicTest()
        {
            var result = FindMissingNumber(new[] {1, 2, 3, 7, 8, 9, 6, 4, 10});

            Assert.AreEqual(5,result);
        }

        [Test]
        public void AdditionalTest1()
        {
            var result = FindMissingNumber(new[] {1});

            Assert.AreEqual(null, result);
        }

        [Test]
        public void AdditionalTest2()
        {
            var result = FindMissingNumber(new[] {1});

            Assert.AreEqual(null, result);
        }


    }
}
