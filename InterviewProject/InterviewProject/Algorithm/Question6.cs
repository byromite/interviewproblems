﻿using NUnit.Framework;

namespace InterviewProject.Algorithm
{
    [TestFixture]
    public class Question6
    {
        /*
         * We need to find if a string is an anagram of another (uses all the original letters exactly once. ex: binary / brainy
         * a. Write a method that accepts 2 strings and returns true if they are anagrams of each other, false otherwise
         * b. Get BasicTest to pass
         * c. Create additional tests to explore more scenarios
         *
         */
        public static bool IsAnagram(string str1, string str2)
        {

            return false;
        }

        [Test]
        public void BasicTest()
        {
            var result = IsAnagram("binary", "brainy");

            Assert.IsTrue(result);
        }

        [Test]
        public void AdditionalTest1()
        {
            var result = IsAnagram("", "");

            Assert.IsTrue(result);
        }

        [Test]
        public void AdditionalTest2()
        {
            var result = IsAnagram("", "");

            Assert.IsTrue(result);
        }
    }
}
