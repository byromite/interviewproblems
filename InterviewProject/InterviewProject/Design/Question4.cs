﻿using NUnit.Framework;

namespace InterviewProject.Design
{
    [TestFixture]
    public class Question4
    {
        // a.  Write a class called Animal, this class should have a method called "MakeNoise" and "Move" that return a string.
        //         There should be no way to make an instance of this class
        // b.  Create a class called Dog and Bird.  These are both types of animals.
        //         When a dog makes a noise, it should return "Bark", when it moves, it should return "I Run".
        //         When a bird makes a noise, it should return "Squawk", when it moves, it should return "I Fly"
        // c.  Create tests to validate that all classes/methods are working as expected.
        
        [Test]
        public void BasicTest()
        {
            
        }
    }
}